CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
---------------
This module creates a new field called wikiloc so you can show map routes
from http://es.wikiloc.com/

INSTALLATION
---------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

1. Extract module archive in "sites/all/modules".
2. Enable module "WikiLoc".
3. Add the new field.

CONFIGURATION
---------------
In Structure>Content Types>Type>Manage Fields add new field type wikiloc
and that's all ;)

MAINTENERS
---------------
Javier Maties (jmaties) https://www.drupal.org/user/63137
